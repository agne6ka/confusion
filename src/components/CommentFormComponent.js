import React, { Component } from 'react';
import { Button, Row, Col, Label, Modal, ModalHeader, ModalBody } from 'reactstrap';
import { Control, LocalForm, Errors } from 'react-redux-form';

class CommentForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false
    }
    this.toggleModal = this.toggleModal.bind(this);
  }

  handleSubmit(values) {
    this.props.postComment(this.props.dishId, values.rating, values.comment, values.author);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }

  render() {
    const required = (val) => val && val.length;
    const maxLength = (len) => (val) => !(val) || (val.length <= len);
    const minLength = (len) => (val) => val && (val.length >= len);

    return (
      <div className="m-3">
        <Button outline onClick={this.toggleModal}><span className="fa fa-edit fa-lg"></span>Submit Comment</Button>
        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
           <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
           <ModalBody>
           <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
               <Row className="form-group">
                   <Label htmlFor="rating" md={12}>Rating</Label>
                   <Col md={12}>
                       <Control.select model=".rating" id="rating" name="rating" placeholder="Rating" className="form-control">
                         <option value="1">1</option>
                         <option value="2">2</option>
                         <option value="3">3</option>
                         <option value="4">4</option>
                         <option value="5">5</option>
                       </Control.select>
                   </Col>
                </Row>
               <Row className="form-group">
                   <Label htmlFor="author" md={12}>Your Name</Label>
                   <Col md={12}>
                       <Control.text model=".author" id="author" name="author"
                           placeholder="Author"
                           className="form-control"
                           validators={{
                               required,
                               minLength: minLength(3),
                               maxLength: maxLength(15)
                           }}
                         />
                         <Errors
                            className="text-danger"
                            model=".author"
                            show="touched"
                            messages={{
                                required: 'Required',
                                minLength: 'Must be greater than 2 characters',
                                maxLength: 'Must be 15 characters or less'
                            }}
                         />
                   </Col>
                </Row>
                <Row className="form-group">
                    <Label htmlFor="comment" md={12}>Comment</Label>
                    <Col md={12}>
                        <Control.textarea model=".comment" id="comment" name="comment"
                            rows="12"
                            className="form-control"
                            validators={{
                                required,
                                minLength: minLength(3),
                                maxLength: maxLength(50)
                            }}
                          />
                          <Errors
                             className="text-danger"
                             model=".firstname"
                             show="touched"
                             messages={{
                                 required: 'Required',
                                 minLength: 'Must be greater than 2 characters',
                                 maxLength: 'Must be 15 characters or less'
                             }}
                          />
                    </Col>
                </Row>
                <Row className="form-group">
                    <Col md={{size:12}}>
                        <Button type="submit" color="primary">
                        Submit
                        </Button>
                    </Col>
                </Row>
              </LocalForm>
           </ModalBody>
       </Modal>
      </div>
    );
  }
}

export default CommentForm;
