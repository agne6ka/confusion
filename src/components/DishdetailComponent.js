import React from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { ListGroup, ListGroupItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import CommentForm from './CommentFormComponent';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
/**
** Created a new DishdetailComponent and added it to your React application.
** Updated the view of the DishdetailComponent to display the details of the selected
   dish using an reactstrap card component.
** Updated the view of the DishdetailComponent to display the list of comments about
   the dish using the Bootstrap unstyled list component.
**/

function RenderDish({dish}) {
  if (dish != null) {
    return (
      <Card>
        <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name} />
        <CardBody>
          <CardTitle>{dish.name}</CardTitle>
          <CardText>{dish.description}</CardText>
        </CardBody>
      </Card>
    )
  } else {
    return (
      <div></div>
    )
  }
}

function RenderComments({comments, postComment, dishId}) {
  if (comments != null) {
    let oneComment = comments.map((comment) => {
      return (
        <ListGroup key={comment.id}>
          <ListGroupItem>
              {comment.comment}<br />
              <small>--
                {comment.author},
                {new Intl.DateTimeFormat('en-GB', {
                  year: 'numeric',
                  month: 'long',
                  day: '2-digit'
                }).format(new Date(comment.date))}
              </small>
          </ListGroupItem>
        </ListGroup>
      );
    });
    return (
        <div className="comments">
          <h4>Comments</h4>
          {oneComment}
          <CommentForm dishId={dishId} postComment={postComment} />
        </div>
    )
  } else {
    return (
      <div></div>
    )
  }
}

const DishDetail = (props) => {
  if(props.isLoading) {
    return(
      <div className="container">
          <div className="row">
              <Loading />
          </div>
      </div>
    )
  }
  else if (props.errMess) {
    return(
       <div className="container">
           <div className="row">
               <h4>{props.errMess}</h4>
           </div>
       </div>
    );
  }
  else {
    return(
      <div className="container">
        <div className="row">
          <Breadcrumb>
            <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
            <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
          </Breadcrumb>
          <div className="col-12">
            <h3>{props.dish.name}</h3>
            <hr />
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-md-5 m-1">
            <RenderDish dish={props.dish}/>
          </div>
          <div className="col-12 col-md-5 m-1">
            <RenderComments comments={props.comments}
              postComment={props.postComment}
              dishId={props.dish.id}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default DishDetail;
